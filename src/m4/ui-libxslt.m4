dnl AM_PATH_XSLT(MINIMUM-VERSION)
dnl
dnl Simple test for libxslt1 with min version. Defines XSLT_CFLAGS and XSLT_LIBS.
dnl
AC_DEFUN([AM_PATH_XSLT],
[
	AC_PATH_PROG(XSLT_CONFIG, xslt-config, no)
	if test "x$XSLT_CONFIG" = "xno"; then
		AC_MSG_ERROR(["xslt-config not found, libxslt1 development package missing"])
	fi
	xslt_version_major=`${XSLT_CONFIG} --version | cut -d. -f1`
	xslt_version_minor=`${XSLT_CONFIG} --version | cut -d. -f2`
	xslt_version_patch=`${XSLT_CONFIG} --version | cut -d. -f3`

  AC_MSG_CHECKING(for libxslt - version >= $1)
	xslt_requiredversion_major=`echo -n $1 | cut -d. -f1`
	xslt_requiredversion_minor=`echo -n $1 | cut -d. -f2`
	xslt_requiredversion_patch=`echo -n $1 | cut -d. -f3`

	if ! test ${xslt_version_major} -ge ${xslt_requiredversion_major} -a ${xslt_version_minor} -ge ${xslt_requiredversion_minor} -a ${xslt_version_patch} -ge ${xslt_requiredversion_patch}; then
		AC_MSG_ERROR(["We need at least version ${xslt_requiredversion_major}.${xslt_requiredversion_minor}.${xslt_requiredversion_patch} of libxslt1"])
	fi
	XSLT_LIBS=`${XSLT_CONFIG} --libs`
	XSLT_CFLAGS=`${XSLT_CONFIG} --cflags`
	AC_SUBST(XSLT_LIBS)
	AC_SUBST(XSLT_CFLAGS)
	AC_MSG_RESULT(yes (version $xslt_version_major.$xslt_version_minor.$xslt_version_patch))
])
