dnl Initialize an ui-auto project
dnl
dnl Usage:
dnl UI_INIT(SOURCE_FILE, [SO_VERSION=], [TOOLDIR=src/tools], [INCDIR=src/PACKAGE], [AUTOMAKE_OPTS=])
dnl
dnl SOURCE_FILE   (mandatory)     See AC_CONFIG_SRCDIR.
dnl SO_VERSION    (optional)      Give SO version (libtool style: e.g. 2:4:0) if needed.
dnl TOOLDIR       (sane default)  UI-automagic script PACKAGE-version will be created in that directory.
dnl INCDIR        (sane default)  UI-automagic header version.h will be created in that directory.
dnl AUTOMAKE_OPTS (optional)      Options to AM_INIT_AUTOMAKE.
dnl
AC_DEFUN([UI_INIT], [
dnl
dnl Generic autoconf/automake stuff
dnl
	AC_PREREQ(2.58)
	AC_CONFIG_SRCDIR($1)
	AM_INIT_AUTOMAKE($5)

dnl
dnl Basic initializations
dnl
	if test -z "${PACKAGE_VERSION}"; then
		AC_MSG_ERROR([[Cant continue: PACKAGE_VERSION missing.]])
	fi
	SO_VERSION=$2
	AC_SUBST(SO_VERSION)

	TOOLDIR=$3
	if test -z "${TOOLDIR}"; then
		TOOLDIR="src/tools"
	fi

	INCDIR=$4
	if test -z "${INCDIR}"; then
		INCDIR="src/${PACKAGE_NAME}"
	fi

dnl
dnl Initialize ui-auto.am
dnl
	UI_AUTO_AM="ui-auto.am"
	rm -v -f "${UI_AUTO_AM}"
	AC_SUBST_FILE(UI_AUTO_AM)

	UI_ADD_AM([
# These are the generic ui-auto rules rules (add these to your Makefile.am appropriately to *-local targets).
.PHONY: ui-auto-all ui-auto-clean ui-auto-dist ui-auto-distclean ui-auto-install-data ui-auto-install-exec ui-auto-uninstall

ui-auto-am-clean:
	rm -f "${UI_AUTO_AM}"

.PHONY: ui-auto-am-clean

# Set dependencies for generic rules
ui-auto-distclean: ui-auto-am-clean
])

dnl
dnl PACKAGE-version script (mandatory)
dnl
	S_NAME="${PACKAGE_NAME}-version"
	S_PATH="${TOOLDIR}/${S_NAME}"
	V_MAJOR=`echo ${PACKAGE_VERSION} | cut -d'.' -f1`
	V_MINOR=`echo ${PACKAGE_VERSION} | cut -d'.' -f2`
	V_PATCH=`echo ${PACKAGE_VERSION} | cut -d'.' -f3`

	mkdir -p `dirname "${S_PATH}"`
	cat <<EOF >"${S_PATH}"
#!/bin/sh -e
echo "UI_PACKAGE=\"${PACKAGE_NAME}\""
echo "UI_VERSION=\"${PACKAGE_VERSION}\""
echo "UI_VERSION_MAJOR=\"${V_MAJOR}\""
echo "UI_VERSION_MINOR=\"${V_MINOR}\""
echo "UI_VERSION_PATCH=\"${V_PATCH}\""
EOF

	if test -n "${SO_VERSION}"; then
		LV_IF=`echo ${SO_VERSION} | cut -d':' -f1`
		LV_REV=`echo ${SO_VERSION} | cut -d':' -f2`
		LV_AGE=`echo ${SO_VERSION} | cut -d':' -f3`
		LV_MAJOR=$[[LV_IF - LV_AGE]]
		LV_MINOR=${LV_AGE}
		LV_PATCH=${LV_REV}
		SO_VERSION_SYS="${LV_MAJOR}.${LV_MINOR}.${LV_PATCH}"

		cat <<EOF >>"${S_PATH}"
echo "UI_SO_VERSION=\"${SO_VERSION_SYS}\""
echo "UI_SO_VERSION_MAJOR=\"${LV_MAJOR}\""
echo "UI_SO_VERSION_MINOR=\"${LV_MINOR}\""
echo "UI_SO_VERSION_PATCH=\"${LV_PATCH}\""
EOF
	fi
	chmod +x "${S_PATH}"

dnl Man page for version script
	S_NAME_CAP=`echo "${S_NAME}" | tr "[[:lower:]]" "[[:upper:]]"`
	cat <<EOF >"${S_PATH}.1"
.TH ${S_NAME_CAP} "1"
.SH NAME
${S_NAME} \- Print parsable version information for development package ${PACKAGE_NAME}.
.SH SYNOPSIS
.PP
.B ${S_NAME}
.SH DESCRIPTION
.PP
Prints version information for installed development package
${PACKAGE_NAME} in a parsable way.

This is intended to be used by automatic version checks of build systems.
.SH AUTHOR
EOF
	if test -n "${SOURCE_DATE_EPOCH}"; then \
		echo "Produced automatically by ui-auto for ${PACKAGE_NAME} on `date --utc --rfc-822 --date="@${SOURCE_DATE_EPOCH}"`." >>${S_PATH}.1
	else \
		echo "Produced automatically by ui-auto for ${PACKAGE_NAME} on `date --rfc-822`." >>${S_PATH}.1
	fi

	dnl Automake rules for installation && distclean
	UI_ADD_AM([
ui-version-script-install:
	install -d -m755 \"\$(DESTDIR)\$(bindir)\"
	install -m755 \"${S_PATH}\" \"\$(DESTDIR)\$(bindir)\"
	install -d -m755 \"\$(DESTDIR)\$(mandir)/man1\"
	install -m644 \"${S_PATH}.1\" \"\$(DESTDIR)\$(mandir)/man1\"

ui-version-script-uninstall:
	rm -f \"\$(DESTDIR)\$(bindir)/${S_NAME}\" \"\$(DESTDIR)\$(mandir)/man1/${S_NAME}.1\"

ui-version-script-clean:
	rm -f "${S_PATH}" "${S_PATH}.1"

.PHONY: ui-version-script-install ui-version-script-uninstall ui-version-script-clean

# Set dependencies for generic rules
ui-auto-distclean: ui-version-script-clean
ui-auto-install-exec: ui-version-script-install
ui-auto-uninstall: ui-version-script-uninstall
])

dnl
dnl version.h include (only when lib)
dnl
	if test -n "${SO_VERSION}"; then
		I_NAME="version.h"
		I_PATH="${INCDIR}/${I_NAME}"
		HEADER_NAME=`echo "${PACKAGE_NAME}" | tr - _ | tr [a-z] [A-Z]`
		mkdir -p `dirname "${I_PATH}"`
		cat <<EOF >"${I_PATH}"
#ifndef ${HEADER_NAME}_VERSION_H
#define ${HEADER_NAME}_VERSION_H

/** @{ @brief ui-auto'mated version macros. */
#define ${HEADER_NAME}_VERSION "${PACKAGE_VERSION}"
#define ${HEADER_NAME}_VERSION_MAJOR ${V_MAJOR}
#define ${HEADER_NAME}_VERSION_MINOR ${V_MINOR}
#define ${HEADER_NAME}_VERSION_PATCH ${V_PATCH}
/** @} */
/** @{ @brief ui-auto'mated library version support. */
#define ${HEADER_NAME}_LIBVERSION "${SO_VERSION_SYS}"
#define ${HEADER_NAME}_LIBVERSION_MAJOR ${LV_MAJOR}
#define ${HEADER_NAME}_LIBVERSION_MINOR ${LV_MINOR}
#define ${HEADER_NAME}_LIBVERSION_PATCH ${LV_PATCH}
/** @} */
#endif
EOF
	dnl Automake rules for installation && distclean
	UI_ADD_AM([
ui-version-header-install:
	install -d -m755 \"\$(DESTDIR)\$(incdir)\"
	install -m644 \"${I_PATH}\" \"\$(DESTDIR)\$(incdir)\"

ui-version-header-uninstall:
	rm -f \"\$(DESTDIR)\$(incdir)/${I_NAME}\"

ui-version-header-clean:
	rm -f "${I_PATH}"

.PHONY: ui-version-header-install ui-version-header-uninstall ui-version-header-clean

# Set dependencies for generic rules
ui-auto-distclean: ui-version-header-clean
ui-auto-install-exec: ui-version-header-install
ui-auto-uninstall: ui-version-header-uninstall
])
	fi
])

dnl Check for ui-auto enabled project.
dnl
dnl Usage:
dnl UI_CHECK(key, package, MAJOR,MINOR,PATCH, LMAJOR,LMINOR,LPATCH)
dnl
dnl key         unique key for this test
dnl package     package name (script name searched for is PACKAGE-version)
dnl MAJOR       (optional) required major version number of library
dnl MINOR       (optional) required minor version number of library
dnl PATCH       (optional) required patchlevel of library
dnl LMAJOR      (optional) required major version number of library interface
dnl LMINOR      (optional) required minor version number of library interface
dnl LPATCH      (optional) required patchlevel of library interface
dnl
dnl All version numbers default to 0 if not specified.
dnl
AC_DEFUN([UI_CHECK], [
	ifelse($3, , MAJOR=0, MAJOR=$3)
	ifelse($4, , MINOR=0, MINOR=$4)
	ifelse($5, , PATCH=0, PATCH=$5)
	ifelse($6, , LMAJOR=0, LMAJOR=$6)
	ifelse($7, , LMINOR=0, LMINOR=$7)
	ifelse($8, , LPATCH=0, LPATCH=$8)

	dnl Check and evaluate version script
	AC_PATH_PROG([$1], [$2-version])
	test x"$[$1]" = x && AC_MSG_ERROR([$2-version not found in PATH])

	dnl Reset variables
	UI_PACKAGE=""
	UI_VERSION=""
	UI_VERSION_MAJOR=""
	UI_VERSION_MINOR=""
	UI_VERSION_PATCH=""
	UI_SO_VERSION=""
	UI_SO_VERSION_MAJOR=""
	UI_SO_VERSION_MINOR=""
	UI_SO_VERSION_PATCH=""

	dnl Set variables from PACKAGE-version script
	eval `[$2-version]`
	dnl Sanity check
	test x"${UI_PACKAGE}" = x[$2] || AC_MSG_ERROR([$2-version found in PATH seems corrupt; from old unstable ui-auto version? ])

	dnl Check package version
	AC_MSG_CHECKING([package $2, version ${MAJOR}.${MINOR}.${PATCH} or later])

	for TYPE in MAJOR MINOR PATCH; do
		INSTALLED_VERSION="UI_VERSION_${TYPE}"
		NEEDED_VERSION="${TYPE}"
		if test ${!INSTALLED_VERSION} -lt ${!NEEDED_VERSION}; then
			AC_MSG_ERROR([${UI_VERSION} is too old.])
		elif test ${!INSTALLED_VERSION} -gt ${!NEEDED_VERSION}; then
			break
		fi
	done
	AC_MSG_RESULT([yes (version ${UI_VERSION})])

	dnl Check library version
	if test -n "${UI_SO_VERSION}"; then
		AC_MSG_CHECKING([library from ${UI_PACKAGE}, version ${LMAJOR}.${LMINOR}.${LPATCH} or later])
		for TYPE in MAJOR MINOR PATCH; do
			INSTALLED_VERSION="UI_SO_VERSION_${TYPE}"
			NEEDED_VERSION="L${TYPE}"
			if test ${!INSTALLED_VERSION} -lt ${!NEEDED_VERSION}; then
				AC_MSG_ERROR([${UI_SO_VERSION} is too old.])
			elif test ${!INSTALLED_VERSION} -gt ${!NEEDED_VERSION}; then
				break
			fi
		done
		AC_MSG_RESULT([yes (lib version ${UI_SO_VERSION})])
	fi
])
