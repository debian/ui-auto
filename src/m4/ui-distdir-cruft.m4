# Clean distribution dir from "well-known" cruft.
#
# Usage configure.ac: UI_DISTDIR_CRUFT
# Usage Makefile.am : dist-hook: ui-auto-dist
AC_DEFUN([UI_DISTDIR_CRUFT],
[
	UI_ADD_AM([
ui-distdir-cruft:
	test ! -z \$(distdir)
	rm -rf \`find \$(distdir) -type d -name "CVS" -o -name ".svn" -o -name ".git"\`
	rm -f \`find \$(distdir) -type f -name ".cvsignore" -or -name CVSDUMMY -or -name '*~' -or -name '#*#' -or -name '.#*'\`

.PHONY: ui-distdir-cruft

# Set dependencies for generic rules
ui-auto-dist: ui-distdir-cruft
])
])
