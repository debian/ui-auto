AC_DEFUN([UI_APACHE_MODULE],
[
AC_ARG_VAR([APXS], [path to apache extension tool used])
AC_ARG_WITH(
	[apxs],
	[AS_HELP_STRING([--with-apxs], [path to apache extension tool used.])],
	[
		echo -n "checking for apxs... "
		if test ! -x "$with_apxs"
		then
			echo "no ($with_apxs is not executable)"
		else
			echo "$with_apxs"
			APXS="$with_apxs"
		fi
	],
	[AC_PATH_PROG([APXS], [apxs])]
)

if test "x$APXS" = "x"
then
	AC_MSG_ERROR([The apache extension tool (apxs) was not found in your PATH. You
	          might want to specify the correct path using the `--with-apxs' switch.])
fi

dnl Flags to compile the object file as apxs would
UI_APACHE_MODULE_CFLAGS="`$APXS -q CFLAGS` `$APXS -q CFLAGS_SHLIB` -I`$APXS -q INCLUDEDIR`"
AC_SUBST([UI_APACHE_MODULE_CFLAGS])

dnl Flags to build the library as apxs would
UI_APACHE_MODULE_LDFLAGS="`$APXS -q LDFLAGS_SHLIB`"
UI_APACHE_MODULE_LIBS="`$APXS -q LIBS_SHLIB`"
AC_SUBST([UI_APACHE_MODULE_LDFLAGS] [UI_APACHE_MODULE_LIBS])

dnl Set libdir only if nothing was specified by the user
if test "$prefix" = "NONE" && test "$exec_prefix" = "NONE" && test $libdir = '${exec_prefix}/lib'
then
	libdir="`$APXS -q LIBEXECDIR`"
fi
])
