AC_DEFUN([UI_DOXYGEN],
[
AC_ARG_ENABLE(ui-doxygen,[
  --disable-ui-doxygen    Disable generation of doxygen documentation.],
[
UI_DOXYGEN_ENABLED="${enableval}"
])

UI_DOXYGEN_TARGETS='def html latex man rtf xml'
if test "$UI_DOXYGEN_ENABLED" = "no"; then
	UI_DOXYGEN_MAKE="mkdir autodoc && for d in ${UI_DOXYGEN_TARGETS}; do echo 'Doxygen documentation was disabled by configure option.' >autodoc/\$\${d}; done"
else
	UI_PATH_PROG(DOXYGEN, doxygen)
	UI_PATH_PROG(PERL, perl)
	UI_DOXYGEN_MAKE='$(DOXYGEN) Doxyfile'
fi

UI_ADD_AM([

ui-doxygen:
	rm -rf autodoc && ${UI_DOXYGEN_MAKE}
	touch ui-doxygen

ui-doxygen-clean:
	rm -rf autodoc ui-doxygen

ui-doxygen-install: ui-doxygen
	install -d \$(DESTDIR)\$(docdir) && \
	chmod -R u=rwx,g=rx,o=rx \$(DESTDIR)\$(docdir) && \
	for d in ${UI_DOXYGEN_TARGETS}; do \
		cp -a autodoc/\$\${d} \$(DESTDIR)\$(docdir) 2>/dev/null || true ; \
	done

ui-doxygen-uninstall:
	for d in ${UI_DOXYGEN_TARGETS}; do \
		chmod -R u+wx \$(DESTDIR)\$(docdir)/\$\${d} 2>/dev/null || true ; \
		rm -rf \$(DESTDIR)\$(docdir)/\$\${d}; \
	done

.PHONY: ui-doxygen-clean ui-doxygen-install ui-doxygen-uninstall

ui-auto-all: ui-doxygen
ui-auto-clean: ui-doxygen-clean
ui-auto-install-data: ui-doxygen-install
ui-auto-uninstall: ui-doxygen-uninstall

])
])
