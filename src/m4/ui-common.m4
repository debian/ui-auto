dnl UI common autotools helper macros

dnl UI_MSG_MISSING(VALUE, WHAT [,"warn"])
dnl
dnl If VALUE is empty or "no, exit with error message that suport
dnl for WHAT is missing, or continue with a warning if "warn" is given.
AC_DEFUN([UI_MSG_MISSING],
[
	if test x"[$1]" = "x" -o x"[$1]" = "xno"; then
		if test x"[$3]" = "xwarn"; then
			AC_MSG_WARN([Missing support for: $2.])
		else
			AC_MSG_ERROR([Missing support for: $2.])
		fi
	fi
])

dnl Simplified and enhanced version of AC_PATH_PROG
dnl
dnl UI_PATH_PROG(VARIABLE, PROG-TO-CHECK-FOR [, "warn"])
dnl
dnl Check for program with AC_PATH_PROG. If program is not found, exit with
dnl error unless "warn" option is given.
dnl
AC_DEFUN([UI_PATH_PROG],
[
	AC_PATH_PROG([$1], [$2])
	UI_MSG_MISSING([$$1], [Program $2], [$3])
])

dnl Echo to file
dnl
dnl UI_PRINT_FILE(FILENAME, TEXT)
AC_DEFUN([UI_PRINT_FILE],
[
	echo -n "$2" >"$1"
])

dnl Add automake source
dnl
dnl UI_ADD_AM(makefile code)
AC_DEFUN([UI_ADD_AM],
[
	echo -n "$1" >>"ui-auto.am"
])

dnl ui-auto check for C++ compilers
dnl
dnl Calls AC_PROG_CXX, and then checks for some compilers/versions we must not use.
dnl
dnl UI_ADD_AM(makefile code)
AC_DEFUN([UI_PROG_CXX],
[
	AC_PROG_CXX
	if test "x$GXX" = "xyes"; then
		if ${CXX} --version | grep "2\.9[[]0123456789[]]\..*"; then
			AC_MSG_ERROR([We cannot use g++-2.9*; pls use at least g++-3.0.])
		fi
	fi
])
