#----------------------------------------------------------------------------
# [ui-auto-release Debian configuration: Template for svn+mini-buildd 0.8.x]
#----------------------------------------------------------------------------

echo "W: mini-buildd 0.8.x is obsoleted by 1.0.x. Use 'template.deb_svn+mbd-1.0' for 1.0.x" >&2

# From mini-buildd, so we can do a little magic for the revapx
__mbdBasedist2Version()
{
	# Known base distributions
	local woody=30
	local sarge=31
	local etch=40
	local lenny=50
	local squeeze=60
	local wheezy=70
	local jessie=80
	local stretch=90
	local buster=100
	local bullseye=11
	local bookworm=12
	local trixie=~TRIXIE
	local forky=~FORKY
	local sid=~SID

	local version=${!1}
	if [ -z "${version}" ]; then
		echo "W: Template svn+mbd: Unknown base dist ${1}." >&2
	fi
	echo -n "${version}"
}

__VCLOC="${1}"
__MBDID="${2}"
__BASEDIST="${3}"
__BPDISTS="${4}"

__REVAPX="~${__MBDID}$(__mbdBasedist2Version ${__BASEDIST})"
for __d in ${__BPDISTS}; do
	__BPDISTS_S="${__d}-${__MBDID},${__BPDISTS_S}"
	__BPDISTS_U="${__d}-${__MBDID}-experimental,${__BPDISTS_U}"
done

# Usage check
if [ -z "${__VCLOC}" -o -z "${__MBDID}" -o -z "${__BASEDIST}" ]; then
	echo "E: Wrong template args. Usage: . template.deb_svn+deb VCLOC MBDID BASEDIST [BPDISTS]" >&2
	exit 1
fi

# Debian dir: We use svn-buildpackage default layout.
ui_release_deb_vc="svn"
ui_release_deb_vc_loc="${__VCLOC}"
ui_release_deb_orig_loc="tarballs"
ui_release_deb_pkg_loc="build-area"

# Common values for all Debian releases ([un]stable[snapshot])
ui_release_deb_vc_tag="trunk"
ui_release_deb_dbuild="svn-buildpackage -rfakeroot -S"
ui_release_deb_dput="mini-buildd-${__MBDID}"

# Values for "unstable"
ui_release_deb_dbuild_options_unstable="--svn-tag"
ui_release_deb_dist_unstable="${__BASEDIST}-${__MBDID}-experimental"
ui_release_deb_revapx_unstable="${__REVAPX}+0"
[ -z "${__BPDISTS_U}" ] || ui_release_deb_clentries_unstable="MINI_BUILDD: AUTO_BACKPORTS: ${__BPDISTS_U}"
# Don't tag for snapshots, and ignore the never-checked-in changes
ui_release_deb_dbuild_options_unstable_snapshot="--svn-ignore"

# Values for "stable"
ui_release_deb_dbuild_options_stable="--svn-tag"
ui_release_deb_dist_stable="${__BASEDIST}-${__MBDID}"
ui_release_deb_revapx_stable="${__REVAPX}+1"
[ -z "${__BPDISTS_S}" ] || ui_release_deb_clentries_stable="MINI_BUILDD: AUTO_BACKPORTS: ${__BPDISTS_S}"
# Treat stable snapshots like unstable snapshots
ui_release_deb_dbuild_options_stable_snapshot="--svn-ignore"
ui_release_deb_dist_stable_snapshot="${__BASEDIST}-${__MBDID}-experimental"
ui_release_deb_revapx_stable_snapshot="${__REVAPX}+0"
[ -z "${__BPDISTS_U}" ] || ui_release_deb_clentries_stable_snapshot="MINI_BUILDD: AUTO_BACKPORTS: ${__BPDISTS_U}"
