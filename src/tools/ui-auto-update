#!/bin/bash -e

PATH="${PATH}:$(dirname $0):/usr/local/share/ui-auto:/usr/share/ui-auto"
. ui-libopt.sh

ui_opt_init "Update ui-auto enabled projects."
ui_opt_add "d" "Also run for the project's dependencies."
ui_opt_add "D" "Only run for the project's dependencies."
ui_opt_add "c" "Also clean project(s)."
ui_opt_add "u" "Also update vc working directories."
ui_opt_add "t" "Test: Also run the 'check' target."
ui_opt_add "T" "Test: Also run the 'distcheck' target."
ui_opt_add "a" "'All project updates' shortcut." "" "c u"
ui_opt_add "A" "'All project updates w/ dependencies' shortcut." "" "d c u"
ui_opt_addPos ID "A project id as configured in '~/.ui-auto.conf'."
ui_opt_parse "$@"

serialCall()
{
	local list="${1}"

	# Handle down all options but dependency handling
	local opts=$(ui_opt_assemble "c u t T")

	for p in ${1}; do
		ui-auto-update ${opts} "${p}"
	done
}

# Processing starts here
if ! . ~/.ui-auto.conf; then
	ui_opt_error " Wrong syntax in (or no) '~/.ui-auto.conf' file; please configure first"
fi
ID=$(ui_opt_getPos 0 | tr "-" "_")
ID_LOC="${ID}_loc"
if [ -z "${!ID_LOC}" ]; then
	echo -e "ID $ID not configured in ~/.ui-auto.conf.\n" >&2
	exit 1
fi
ID_DEPS="${ID}_deps"
ID_CONF="${ID}_conf"

if ui_opt_given d; then
	serialCall "${!ID_DEPS} ${ID}"
	exit 0
elif ui_opt_given D; then
	serialCall "${!ID_DEPS}"
	exit 0
fi

# Start processing
echo "=> Updating: ${ID}"

cd "${!ID_LOC}"

# Set environment for all dependencies
eval $(ui-auto-env -D "${ID}")

# Update && clean options
if ui_opt_given c; then
	ui-auto-ubs strap C
fi
if ui_opt_given u; then
	ui-auto-uvc update
fi

# Standard
ui-auto-ubs strap
ui-auto-ubs configure "${!ID_CONF}"
ui-auto-ubs build

# Tests
if ui_opt_given t; then
	ui-auto-ubs check
fi
if ui_opt_given T; then
	ui-auto-ubs distcheck
fi

echo "=> Updated: ${ID}"
exit 0
