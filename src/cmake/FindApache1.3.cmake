# - Find APACHE1_3
# Find the native APACHE1_3 includes and library
# This module defines
#  APACHE1_3_INCLUDE_DIR, where to find apache-1.3/httpd.h, etc.
#  APACHE1_3, the libraries needed to use DB.
#  APACHE1_3, If false, do not try to use DB.

find_path(APACHE1.3_INCLUDE_DIR NAMES apache-1.3/httpd.h)
#find_library(APACHE1.3_LIBRARY foo)

#if(APACHE1.3_INCLUDE_DIR AND APACHE1.3_LIBRARY)
if(APACHE1.3_INCLUDE_DIR)
	SET(APACHE1.3_FOUND true)
else()
	SET(APACHE1.3_FOUND false)
endif()


if(APACHE1.3_FOUND)
   if(NOT Apache1.3_FIND_QUIETLY)
#      message(STATUS "Found Apache1_3: ${APACHE1.3_LIBRARY}")
      message(STATUS "Found Apache1.3: ${APACHE1.3_INCLUDE_DIR}")
   endif()
else()
   if(Apache1.3_FIND_REQUIRED)
      message(SEND_ERROR "Could not find Apache1.3")
   endif()
endif()

